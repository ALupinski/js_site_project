$( document ).ready(function() {


    $('#multiselect-platform-add').hover(function () {
        $("#platform-options-add").slideDown('fast');

    }, function () {
        $("#platform-options-add").slideUp('fast');
    });
    $('.platform-label').change(function () {
        if (getCategories().length !== 0){
            $('#category-empty-span').css('display', 'none');

        }
    });
    function getCategories() {
        var selected = [];
        $('#platform-options-add').find('input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        return selected;
    }
    // $('#multiselect-type-add').hover(function () {
    //     $("#type-options-add").slideDown('fast');
    //
    // }, function () {
    //     $("#type-options-add").slideUp('fast');
    // });
    //
    $('#type-input').click(function () {
        if ($(this).val()){
            $('#type-empty-span').css('display', 'none');
        }
    });
    $('#lang-input').click(function () {
        if ($(this).val()){
            $('#type-empty-span').css('display', 'none');
        }
    });
});
