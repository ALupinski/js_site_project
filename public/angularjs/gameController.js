var app = angular.module('your-games',[]);


app.controller('listcontroller',function ($scope,$http) {
    $scope.games={};
    $scope.gatunek={};
    $scope.languagesg={};
    $scope.rating={};
    $scope.platforms={};


    $scope.editedGame={};
    function init(){
        $("#nazwa").text('Wiedźmin 3 Dziki Gon');
        $("#premiera").text('19 maja 2015');

        $("#platforma").text('PC, PS4, Xbox One');

        $("#jezyk").text('EN, PL, DE, FR, RU');
        $("#producent").text('CD Project Red');
        $("#ocena").text("10/10");
        $("#opis").text('Gra stanowiąca trzecią część przygód Geralta z Rivii. Podobnie jak we wcześniejszych odsłonach cyklu, Wiedźmin 3: Dziki Gon bazuje na motywach twórczości literackiej Andrzeja Sapkowskiego, jednak nie jest bezpośrednią adaptacją żadnej z jego książek. Fabuła gry koncentruje się na kilku wątkach dotyczących m.in. utraconej miłości Geralta, inwazji Nilfgaardu na Królestwa Północy oraz tytułowych, mitycznych łowów. Rozgrywka utrzymana jest w sandboksowym stylu, dając graczom dużą swobodę w eksplorowaniu rozległego świata (ponad 40-krotnie większego niż w Wiedźmin 2: Zabójcy Królów) i wykonywaniu setek pobocznych misji. Deweloperzy ze studia CD Projekt RED zadbali o solidną porcję nowości, w tym nowy silnik REDengine3, oferujący znakomitej jakości oprawę audiowizualną.');
        $("#cover-img").attr("src","https://www.gry-online.pl/galeria/gry13/76084339.jpg");

    };
    init();
    $http.get("http://localhost:4200/data")
        .then(function (response) {
            $scope.games = response.data.games;
            //console.log($scope.games);
            $scope.gatunek = response.data.gatunek;
            $scope.languagesg = response.data.languagesg;
            $scope.rating = response.data.rating;
            $scope.platforms = response.data.platforms;

            $scope.editedGame={};
            $scope.saveEdited = function (id) {



                    $http.put("http://localhost:4200/data/" + id, $scope.editedGame, {params: {id: id}})
                        .then(function (response) {
                            console.log("Game edited");
                            $scope.games = response.data.games;
                        }, function (response) {
                            console.log("ERROR - game not  edit")
                        });

                    $scope.reset();


            };
            $scope.delete = function (game) {
                var id =game.id;
                $http.delete("http://localhost:4200/data/"+id,{params: {id: id}})
                    .then(function (response) {
                        console.log("Game deleted");
                        $scope.games = response.data.games;


                    },function (res) {
                    console.log("Game deleted");

                });

            };

            $scope.inspectGame= function(game){
                $("#nazwa").text(game.name);
                $("#premiera").text(game.premiere);
                var plat="";
                (game.platforms).forEach(function (value) {
                    plat += $scope.platforms[value].name + ", ";
                });
                $("#platforma").text(plat);
                var gat="";
                // console.log($scope.gatunek[4]);
                //console.log(game.language);

                (game.gatunki).forEach(function (value) {
                    gat += $scope.gatunek[value] + ", ";
                });
                $("#gatunek").text(gat);
                var lang="";
                (game.languageg).forEach(function(v) {
                    lang += $scope.languagesg[v] + ", ";
                });
                $("#jezyk").text(lang);
                $("#producent").text(game.producer);
                $("#ocena").text(game.rating + "/10");
                $("#opis").text(game.description);
                $("#cover-img").attr("src", game.imgsrc);

            };


        });
    $scope.reloadRoute = function() {
        $route.reload();
    }
    $scope.isAllDataValid = function () {
        var result = true;



        return result;
    };
    $scope.edit = function (game) {

        $scope.editedGame = angular.copy(game);


    };

    $scope.getTemplate = function (game) {
        if (game.id === $scope.editedGame.id) return 'edit';
        else return 'display';
    };
});