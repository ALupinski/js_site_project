var app = angular.module('platformadd',[]);


app.controller('platformaddcontroller',function ($scope,$http) {

    $scope.platforms={};
    $scope.platform={};
    $http.get("http://localhost:4200/data")
        .then(function (response) {

            $scope.platforms = response.data.platforms;

            $scope.add = function (platform) {

                    $http.post("http://localhost:4200/platforms", $scope.platform)
                        .then(function (response) {

                                $scope.platforms = response.data.platforms;
                            },
                            function (response) {
                                console.log("ERROR - platform add")
                            });

                    this.platform = {};
                    $scope.platformForm.$setUntouched();


            };
            $scope.inspectPlatform= function(platform){
                $("#platform-Name").text(platform.name);

                $("#platform-Img").attr("src", platform.href);

            };


        });

    $scope.getTemplate = function (platform) {
        return 'display';
    };
});