var app = angular.module('add-games',[]);

app.controller('listcontrollersec',function ($scope,$http) {
    $scope.games = {};
    $scope.gatunek = {};
    $scope.languagesg = {};
    $scope.rating = {};
    $scope.platforms = {};


    $scope.editedGame = {};
    $scope.game={};
    $http.get("http://localhost:4200/data")
        .then(function (response) {
            $scope.games = response.data.games;
            $scope.gatunek = response.data.gatunek;
            $scope.languagesg = response.data.languagesg;
            $scope.rating = response.data.rating;
            $scope.platforms = response.data.platforms;
            $scope.add = function (games) {
                if ($scope.isAllDataValid()) {

                    $scope.game.platforms = $scope.getPlatforms();
                    $http.post("http://localhost:4200/data", $scope.game)
                        .then(function (response) {
                                console.log("Outfit added");
                                $scope.games = response.data.games;
                            },
                            function (response) {
                                console.log("ERROR - outfit add")
                            });

                    this.game = {};
                    $scope.gameForm.$setUntouched();

                }
            };

            $scope.isAllDataValid = function () {
                var result = true;

                // name validation
                if (!$('#name-input').val()) {
                    if ($('#name-error-span').css('display') === 'none') {
                        $('#name-empty-span').css('display', 'block');
                    } else {
                        $('#name-empty-span').css('display', 'none');

                    }
                    result = false;

                }
                else {
                    $('#name-empty-span').css('display', 'none');

                }

                // premiere validation
                if (!$('#premiere-input').val()) {
                    if ($('#premiere-error-span').css('display') === 'none') {
                        $('#premiere-empty-span').css('display', 'block');
                    } else {
                        $('#premiere-empty-span').css('display', 'none');

                    }
                    result = false;

                }
                else {
                    $('#premiere-empty-span').css('display', 'none');

                }

                // platform validation
                if ($scope.getPlatforms().length === 0) {
                    $('#platform-empty-span').css('display', 'block');
                    result = false;

                } else {
                    $('#platform-empty-span').css('display', 'none');

                }

                // lang validation
                if (!$('#lang-input').val()) {
                    if ($('#lang-error-span').css('display') === 'none') {
                        $('#lang-empty-span').css('display', 'block');
                    } else {
                        $('#lang-empty-span').css('display', 'none');

                    }
                    result = false;

                }
                else {
                    $('#lang-empty-span').css('display', 'none');

                }

                // producer validation
                if (!$('#producer-input').val()) {
                    if ($('#producer-error-span').css('display') === 'none') {
                        $('#producer-empty-span').css('display', 'block');
                    } else {
                        $('#producer-empty-span').css('display', 'none');

                    }
                    result = false;

                }
                else {
                    $('#producer-empty-span').css('display', 'none');

                }
                // rating
                if (!$('#rating-input').val()) {
                    if ($('#rating-error-span').css('display') === 'none') {
                        $('#rating-empty-span').css('display', 'block');
                    } else {
                        $('#rating-empty-span').css('display', 'none');

                    }
                    result = false;

                }
                else {
                    $('#rating-empty-span').css('display', 'none');

                }
                // img validation
                if (!$('#img-input').val()) {
                    if ($('#img-error-span').css('display') === 'none') {
                        $('#img-empty-span').css('display', 'block');
                    } else {
                        $('#img-empty-span').css('display', 'none');

                    }
                    result = false;

                }
                else {
                    $('#producer-empty-span').css('display', 'none');

                }
                return result;
            };
        });

    $scope.getPlatforms = function () {
        var selected = [];
        $('#platform-options-add').find('input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        return selected;
    };
    $scope.getTemplate = function (game) {
        /*if (game.id === $scope.editedGame.id) return 'edit';
        else */
        // console.log(game);
        return 'display';
    };
    $scope.isValidData = function () {
        return !$scope.isEmptyName() && !$scope.isInvalidPrice();
    };
    $scope.isEmptyName = function () {
        return ($('#edit-name').val() === '');
    };
});