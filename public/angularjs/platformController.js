var app = angular.module('platform',[]);


app.controller('platformlistcontroller',function ($scope,$http) {

    $scope.platforms={};
    $scope.editedPlatform={};
    $http.get("http://localhost:4200/platforms")
        .then(function (response) {
            $scope.platforms = response.data.platforms;
            $scope.editedPlatform={};
            $scope.saveEdited = function (id) {

                $http.put("http://localhost:4200/platforms/" + id, $scope.editedPlatform, {params: {id: id}})
                    .then(function (response) {
                        console.log("platform edited");
                        $scope.platforms = response.data.platforms;
                        console.log("platform e");
                    }, function (response) {
                        console.log("ERROR - platform not  edit")
                    });

            };
            $scope.delete = function (platform) {
                var id =platform.id;
                $http.delete("http://localhost:4200/platforms/"+id,{params: {id: id}})
                    .then(function (response) {
                        console.log("platform deleted");
                        $scope.platforms = response.data.platforms;


                    },function (response) {
                        console.log("platform deleted");
                    });
            };
            $scope.inspectPlatform= function(platform){
                $("#platform-Name").text(platform.name);
                $("#platform-Img").attr("src", platform.href);
            };
        });


    $scope.edit = function (platform) {
        $scope.editedPlatform = angular.copy(platform);
    };
    $scope.getTemplate = function (platform) {
        if (platform.id === $scope.editedPlatform.id) return 'edit';
        else return 'display';
    };
});