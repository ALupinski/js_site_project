var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var port = process.env.PORT ||4200;
var app = express();

app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.listen(port, function()  {
  console.log("Listening on port " + port);
});
var languagesg = [
    'EN',
    'PL',
    'DE',
    'FR',
    'RU'
];
var rating = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10'
];
var gatunek = [
    'RPG',
    'Action',
    'Action',
    'Arcade',
    'Fight',
    'Strategy',
    'Adventure',
    'Sport',
    'Racing',
    'Simulation',
    'Logic',
    'MMO'
];
var games=[
    {
        id: 1,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/76084339.jpg",
        name: 'Wiedźmin 3 Dziki Gon',
        premiere: '19 maja 2015',
        platforms: [0,2,9],
        gatunki: [0],
        languageg:[0,1,2,3,4],
        producer: 'CD Project Red',
        rating:[10],
        description:'Gra stanowiąca trzecią część przygód Geralta z Rivii. Podobnie jak we wcześniejszych odsłonach cyklu, Wiedźmin 3: Dziki Gon bazuje na motywach twórczości literackiej Andrzeja Sapkowskiego, jednak nie jest bezpośrednią adaptacją żadnej z jego książek. Fabuła gry koncentruje się na kilku wątkach dotyczących m.in. utraconej miłości Geralta, inwazji Nilfgaardu na Królestwa Północy oraz tytułowych, mitycznych łowów. Rozgrywka utrzymana jest w sandboksowym stylu, dając graczom dużą swobodę w eksplorowaniu rozległego świata (ponad 40-krotnie większego niż w Wiedźmin 2: Zabójcy Królów) i wykonywaniu setek pobocznych misji. Deweloperzy ze studia CD Projekt RED zadbali o solidną porcję nowości, w tym nowy silnik REDengine3, oferujący znakomitej jakości oprawę audiowizualną.'
    },
    {
        id: 2,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/1119867468.jpg",
        name: 'The Elder Scrolls V: Skyrim',
        premiere: '11 listopada 2011',
        platforms: [0,3,7],
        gatunki: [0],
        languageg:[0,1,2,3],
        producer: 'Bethesda Softworks',
        rating:[9],
        description:'The Elder Scrolls V: Skyrim to wyprodukowana przez firmę Bethesda Game Studios piąta odsłona popularnego cyklu cRPG. Akcja gry osadzona została 200 lat po wydarzeniach przedstawionych w The Elder Scrolls IV: Oblivion. Gracze wcielają się w rolę osoby władającej językiem smoków (Dovahkiin) i podążając tropem starożytnej przepowiedni próbują powstrzymać nadejście Alduina, boga zniszczenia, który zagraża całemu kontynentowi Tamriel. Wraz z postępami w grze postać nabywa nowe umiejętności i doskonali już posiadane.'
    },
    {
        id: 3,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/219234890.jpg",
        name: 'Stronghold: Crusader',
        premiere: '24 września 2002',
        platforms: [0],
        gatunki: [6],
        languageg:[0,1,2],
        producer: 'FireFly Studios',
        rating:[7],
        description:'Stronghold: Crusader to sequel świetnej gry strategicznej Stronghold (w Polsce Twierdza), tym razem przenoszący nas do świata Bliskiego Wschodu w epoce wypraw krzyżowych (XI – XII wiek). Podobnie jak poprzednik, Stronghold: Crusader jest określany mianem „symulator twierdzy”. Gracz może od podstaw wybudować swoją twierdzę, zapewnić jej ludności i żołnierzom wyżywienie oraz odeprzeć niejeden potężny atak, czy nawet samemu zdobyć wrogi zamek.'
    },
    {
        id: 4,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/521062578.jpg",
        name: 'Counter Strike: Global Offensive',
        premiere: '21 sierpnia 2012',
        platforms: [0],
        gatunki: [3],
        languageg:[0,1,2,3,4],
        producer: 'Valve Software',
        rating:[7],
        description:'Nowa odsłona klasycznej strzelanki sieciowej, która stała się podstawą współczesnego esportu. Twórcy produkcji starali się wprowadzić sporo usprawnień do rozgrywki i jednocześnie zachować podstawowe cechy pierwowzoru. Gra jest skierowana przede wszystkim do fanów multiplayera i zawiera wiele trybów zabawy. Gracze wcielają się w niej w terrorystów lub antyterrorystów i prowadzą zacięte pojedynki.'
    },
    {
        id: 5,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/1285715234.jpg",
        name: 'Euro Truck Simulator 2',
        premiere: '19 października 2012',
        platforms: [0,3,7],
        gatunki: [9],
        languageg:[0,1,2],
        producer: 'SCS Software',
        rating:[7],
        description:'Następna część znanego symulatora samochodów ciężarowych z akcją rozgrywaną na terenie Europy, autorstwa firmy SCS Software. Wcielamy się w kierowcę pojazdu ciężarowego, a naszym zadaniem jest dostarczenie towaru do miejsca przeznaczenia w jak najkrótszym, optymalnym czasie. '
    },
    {
        id: 6,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/1047487296.jpg",
        name: 'War Thunder',
        premiere: '20 grudnia 2016',
        platforms: [0,3,7],
        gatunki: [9],
        languageg:[0,1,2,3,4],
        producer: 'Gaijin Entertainment',
        rating:[6],
        description:'War Thunder jest grą sieciową wydaną w formule free-to-play pozwalającą na rozegranie historycznych starć z okresu drugiej wojny światowej oraz wojny koreańskiej. Cechą szczególną tytułu jest możliwość wzięcia udziału w bitwach łączonych. Piloci, czołgiści a w przyszłości marynarze staną do walki na jednym polu bitwy, zaś zwycięstwo będzie zależało od współdziałania wszystkich trzech sił: lotnictwa, wojsk pancernych oraz marynarki wojennej.'
    },
    {
        id: 7,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/691066875.jpg",
        name: 'DiRT 3',
        premiere: '24 maja 2011',
        platforms: [0,3,5,7],
        gatunki: [8],
        languageg:[0,1,2,3,4],
        producer: 'Codemasters Software',
        rating:[7],
        description:'Colin McRae: DiRT 3 to kolejna odsłona cyklu gier wyścigowych, sygnowanych nazwiskiem tragicznie zmarłego szkockiego kierowcy rajdowego. W przeciwieństwie do części drugiej, utrzymanej w młodzieżowym, ekstremalnych stylu i kładącej nacisk na zawody offroadowe, w DiRT 3 twórcy zdecydowali się na powrót do klimatów typowo rajdowych.'
    },
    {
        id: 8,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/-736407264.jpg",
        name: 'Gothic II',
        premiere: '29 listopada 2002',
        platforms: [0],
        gatunki: [0],
        languageg:[0,1,2],
        producer: 'Piranha Bytes',
        rating:[8],
        description:'Sequel jednej z najlepszych gier akcji/cRPG roku 2001. Podobnie jak w części poprzedniej akcja Gothic II toczy się w świecie fantasy i kontynuuje fabułę poprzedniczki, tj. wojny pomiędzy siłami zła (cała masa potworów, Orków i Ogrów pod przewodnictwem Smoków) i Ludźmi. Jednak tym razem nie poruszamy się już tylko wewnątrz kolonii przestępców objętej zasięgiem magicznej bariery, a na jej zewnątrz. Akcja toczy się w nowych bardzo rozległych i różnorodnych lokacjach, m.in. miasteczko nadmorskie, klasztor, biblioteka, farma, Wyspa Smoków, zamek oblężony przez Orki'
    },
    {
        id: 9,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/992390.jpg",
        name: 'Call of Duty 2',
        premiere: '25 października 2005',
        platforms: [0,7],
        gatunki: [1],
        languageg:[0,1],
        producer: 'Bethesda Softworks',
        rating:[8],
        description:'Call of Duty 2 to pełnoprawny sequel wydanej pod koniec 2003 roku strzelanki pierwszoosobowej (gry akcji/FPS), z fabułą osadzoną w realiach II Wojny Światowej, która zdobyła mnóstwo nagród i uznanie graczy na całym świecie.'
    },
    {
        id: 10,
        imgsrc:"https://www.gry-online.pl/galeria/gry13/468125765.jpg",
        name: 'Sniper Elite V2',
        premiere: '1 maja 2012',
        platforms: [0,3,7],
        gatunki: [2],
        languageg:[0,1,2,3,4],
        producer: 'Rebellion',
        rating:[6],
        description:'W Sniper Elite V2 wcielamy się w snajpera na usługach rządu amerykańskiego. Scenerią jest zrujnowany przez walczące ze sobą armie - niemiecką i radziecką - Berlin 1945 roku. Zasadniczym elementem rozgrywki zrealizowanej przez twórców z Rebellion jest taktyka łącząca skradanie się z unikaniem niepotrzebnej konfrontacji z wrogiem oraz wybieranie jednej z kilku możliwości przejścia danej misji.'
    },
];
var platforms = [
    {id: 1, name: 'PC',href:'https://www.gry-online.pl/im/plat/1.png'},
    {id: 2, name: 'Android',href:'https://www.gry-online.pl/im/plat/17.png'},
    {id: 3, name: 'PS4',href:'https://www.gry-online.pl/im/plat/15.png'},
    {id: 4, name: 'PS3',href:'https://www.gry-online.pl/im/plat/7.png'},
    {id: 5, name: 'PS2',href:'https://www.gry-online.pl/im/plat/2.png'},
    {id: 6, name: 'PSP',href:'https://www.gry-online.pl/im/plat/8.png'},
    {id: 7, name: 'PS Vita',href:'https://www.gry-online.pl/im/plat/13.png'},
    {id: 8, name: 'Xbox 360',href:'https://www.gry-online.pl/im/plat/6.png'},
    {id: 9, name: 'Xbox',href:'https://www.gry-online.pl/im/plat/3.png'},
    {id: 10, name: 'Xbox One',href:'https://www.gry-online.pl/im/plat/16.png'},
    {id: 11, name: 'Switch',href:'https://www.gry-online.pl/im/plat/21.png'},
    {id: 12, name: 'Wii U',href:'https://www.gry-online.pl/im/plat/14.png'},
    {id: 12, name: 'Wii',href:'https://www.gry-online.pl/im/plat/10.png'}
];
var currentGame = 10;
var currentPlatform = 12;

//--Games--
app.get('/' , function (req, res) {
    res.sendFile('./public/index.html', {root: __dirname});
});

//READ games
app.get('/data', function (req, res) {
    res.json({ games:games, platforms:platforms, gatunek:gatunek,rating:rating,languagesg:languagesg});
    //res.sendFile('./public/index.html', {root: __dirname});

});

//CREATE
app.post('/data', function (req, res) {
    var game = req.body;
    game.id = currentGame++;
    games.push(game);
    res.json({games: games});

});

//DELETE
app.delete('/data/:id', function (req, res) {

    var game = findElement(parseInt(req.params.id), games);
    if (game === null) {
        res.send(404);
    }
    else {
        removeElement(parseInt(req.params.id, 10), games);
        res.json({games: games});

    }
});
//UPDATE
app.put('/data/:id', function (req, res) {
    var game = req.body;
    var currentGame = findElement(parseInt(req.params.id), games);
    if (currentGame === null) {
        res.send(404);
    }
    else {
        currentGame.name = game.name;
        res.send({games: games});
    }
});
//PLATFORMS
//GET
app.get('/platforms', function (req, res) {
    res.json({platforms: platforms});

});

//CREATE
app.post('/platforms', function (req, res) {
    var platform = req.body;
    platform.id = currentPlatform++;
    platforms.push(platform);
    res.json({platforms: platforms});

});

//DELETE
app.delete('/platforms/:id', function (req, res) {

    var platform = findElement(parseInt(req.params.id), platforms);
    if (platform === null) {
        res.send(404);
    }
    else {
        removeElement(parseInt(req.params.id, 10), platforms);
        res.json({platforms: platforms});

    }
});
//UPDATE
app.put('/platforms/:id', function (req, res) {
    var platform = req.body;
    var currentPlatform = findElement(parseInt(req.params.id), platforms);
    if (currentPlatform === null) {
        res.send(404);
    }
    else {
        currentPlatform.name = platform.name;
        res.send({platforms: platforms});
    }
});





//languagesg
//GET
app.get('/languagesg', function (req, res) {
    res.json({languagesg: languagesg});

});
//gatunek
//GET
app.get('/gatunek', function (req, res) {
    res.json({gatunek: gatunek});

});
//rating
//GET
app.get('/rating', function (req, res) {
    res.json({rating: rating});

});

function findElement(id, elementsArray) {
    for (var i = 0; i < elementsArray.length; i++) {
        if (elementsArray[i].id === id) {
            return elementsArray[i];
        }
    }
    return null;
}

function removeElement(id, elementsArray) {
    var elementIndex = 0;
    for (var i = 0; i < elementsArray.length; i++) {
        if (elementsArray[i].id === id) {
            elementIndex = i;
        }
    }
    elementsArray.splice(elementIndex, 1);
}
